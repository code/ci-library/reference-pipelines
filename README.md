# Job Recipes

Collection of jobs which can be used as reference for projects already at Codelinaro or
migrating into it.

# Documentation

- [CodeLinaro Repository and CI Migration Guide](https://docs.google.com/presentation/d/1TMjyj1MWzSmJZrVbJmWdl1Rc1K7vSu0Fv30emB3340Q/edit?usp=sharing). A short guide to migrate projects to CodeLinaro
- [CodeLinaro CI Job Recipes](https://docs.google.com/document/d/1UndrlhWZDC98blAa98LZpFmVEv7Qrp-6eNlCD4XJG7Q/edit?usp=sharing). Ideas about current and possible job recipes

